from setuptools import setup, find_packages

setup(
    name="sa-train",
    version="0.1",
    description="Spamassassin training tools for A/I",
    author="Autistici/Inventati",
    install_requires=["python-gnupg"],
    zip_safe=True,
    packages=find_packages(),
    entry_points={
        "console_scripts": [
            "ai-sa-train = sa_train.train:main",
            "imapexpire = sa_train.util.imapexpire:main",
            "imapsync = sa_train.util.imapsync:main",
        ],
    },
)

