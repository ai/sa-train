#!/bin/bash
#
# Wrapper for sa_train. Invoke with '--docker' to use Docker,
# otherwise it expects a system-wide installation of Spamassassin.
#
# Training data should be stored in mbox files in a data/ subdirectory
# of the one containing this binary.
#

bin_dir=$(dirname "$0")
bin_dir=${bin_dir:-.}
root_dir=${bin_dir}/..

set -u

args=
use_docker=0
while [ $# -gt 0 ]; do
    case "$1" in
        --docker)
            use_docker=1
            ;;
        *)
            args="${args} $1"
            ;;
    esac
    shift
done

sa_train=$(which ai-sa-train)
if [ -z "${sa_train}" ]; then
    sa_train="env PYTHONPATH=${root_dir} python ${root_dir}/sa_train/train.py"
fi
${sa_train} --output_dir=${bin_dir}/data ${args}
if [ $? -gt 0 ]; then
    exit 2
fi

if [ ${use_docker} -eq 1 ]; then
    (cd ${bin_dir} &&
     docker build -t spamtrain . &&
     docker run --name=spamtrain -v ${bin_dir}/data:/data spamtrain /src/learn.sh /data &&
     docker rm spamtrain)
else
    ${bin_dir}/learn.sh ${bin_dir}/data
fi

exit $?

