#!/bin/bash

set -u

bin_dir=$(dirname "$0")
bin_dir=${bin_dir:-.}
data_dir=${1:-../../data}
tmp_dir=$(mktemp -d)
sa_opts="--username global --progress --mbox --siteconfigpath=${tmp_dir}/config"
trap "rm -fr ${tmp_dir}" EXIT

mkdir ${tmp_dir}/db
mkdir ${tmp_dir}/config
(cat ${bin_dir}/../config/local.cf ; echo "bayes_path ${tmp_dir}/db") \
    > ${tmp_dir}/config/local.cf

sa-learn ${sa_opts} --clear

# Automatically parallelize training (by dataset) if the 'parallel'
# command is available.
execfn=/bin/sh
if [ -x /usr/bin/parallel ]; then
    execfn=/usr/bin/parallel
fi
(for f in ${data_dir}/ham*.mbox; do
     test -e $f && echo "sa-learn ${sa_opts} --no-sync --ham $f"
 done
 for f in ${data_dir}/spam*.mbox; do
     test -e $f && echo "sa-learn ${sa_opts} --no-sync --spam $f"
 done) \
    | ${execfn}

sa-learn ${sa_opts} --sync
sa-learn ${sa_opts} --backup > ${data_dir}/sa.dat
