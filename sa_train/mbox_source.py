import logging
import mailbox
import os
import tempfile

log = logging.getLogger(__name__)


class MboxSource(object):
    """Read messages from a (potentially encrypted) mbox file."""

    def __init__(self, kind, mbox):
        self.mbox = mbox
        self.kind = kind

    def close(self):
        pass

    def scan(self):
        for msg in self.mbox:
            log.info('mbox: got msg %s', msg['Message-ID'])
            yield (self.kind, msg)


class TemporaryMboxSource(MboxSource):

    def __init__(self, kind, mbox, path):
        MboxSource.__init__(self, kind, mbox)
        self.path = path

    def close(self):
        MboxSource.close(self)
        os.remove(self.path)


def generic_mbox_source(gpg, kind, path):
    if path.endswith('.mbox'):
        mbox = mailbox.mbox(path, create=False)
        return MboxSource(kind, mbox)
    elif path.endswith('.mbox.gpg'):
        tmpf, tmpname = tempfile.mkstemp()
        os.close(tmpf)
        with open(path, 'r') as fd:
            gpg.decrypt_file(fd, output=tmpname)
        mbox = mailbox.mbox(tmpname, create=False)
        return TemporaryMboxSource(kind, mbox, tmpname)
    else:
        mbox = mailbox.Maildir(path, factory=None)
        return MboxSource(kind, mbox)
