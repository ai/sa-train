import time
import email
import email.utils
import logging
import os
import random
import re
import traceback
from collections import defaultdict
from sa_train import mbox_source

log = logging.getLogger(__name__)


class GPGPipe(object):
    """Decode messages sent to A/I spam-collect address.

    Messages are expected to contain a GPG-encrypted copy of the
    original messages. The wrapper message subject will indicate the
    desired training category (ham or spam).
    """

    def __init__(self, gpg, wrap):
        self.gpg = gpg
        self.wrap = wrap

    def close(self):
        self.wrap.close()

    def scan(self):
        # Ignore upstream kind.
        for _, wrapped in self.wrap.scan():
            kind = wrapped['Subject']
            if kind != 'ham' and kind != 'spam':
                continue

            payload = wrapped.get_payload()
            if not payload:
                continue

            result = self.gpg.decrypt(payload)
            if not result.ok:
                log.error('gpg error: %s\n%s', result.status, result.stderr)
                continue
            orig_msg = email.message_from_string(str(result))

            yield (kind, orig_msg)


class Concatenate(object):

    def __init__(self, sources):
        self.sources = sources

    def close(self):
        pass

    def scan(self):
        for s in self.sources:
            for kind, msg in s.scan():
                yield (kind, msg)
            s.close()


def ArchiveSource(gpg, root_path):
    sources = []
    for kind in ('ham', 'spam'):
        dir = os.path.join(root_path, kind)
        if not os.path.isdir(dir):
            raise Exception('archive at %s does not have {ham,spam} subdirectories')
        for f in os.listdir(dir):
            if f.startswith('.'):
                continue
            path = os.path.join(dir, f)
            s = mbox_source.generic_mbox_source(gpg, kind, path)
            sources.append(s)
    return Concatenate(sources)


class StatsPipe(object):
    """Print statistics about the wrapped stream."""

    def __init__(self, tag, wrap):
        self._wrap = wrap
        self._tag = tag
        self._counters = defaultdict(int)

    def close(self):
        log.info('%s: %s', self._tag, self._counters)
        self._wrap.close()

    def scan(self):
        for kind, msg in self._wrap.scan():
            self._counters[kind] += 1
            yield (kind, msg)


class RandomSample(object):
    """Output a random sample of N items from the input stream."""

    def __init__(self, wrap, n):
        self._wrap = wrap
        self._n = n

    def close(self):
        self._wrap.close()

    def scan(self):
        if self._n == 0:
            return self._wrap.scan()
        return self._random_scan()

    def _random_scan(self):
        cur = []
        i = 0
        for kind, msg in self._wrap.scan():
            i += 1
            if i <= self._n:
                # Keep first n items
                cur.append((kind, msg))
            elif random.randrange(i) < self._n:
                # Keep item
                cur[random.randrange(self._n)] = (kind, msg)
        for kind, msg in cur:
            yield kind, msg


class FilterBase(object):

    def __init__(self, wrap):
        self._wrap = wrap

    def close(self):
        self._wrap.close()

    def scan(self):
        for kind, msg in self._wrap.scan():
            msg, ok = self.check(kind, msg)
            if ok:
                yield kind, msg


class Recent(FilterBase):
    """Filter out old messages."""

    _timespec_rx = re.compile(r'^([0-9]+)([shdwmySHDWMY])?$')

    _mult = {
        's': 1,
        'h': 3600,
        'd': 86400,
        'w': 604800,
        'm': 2592000,
        'y': 31536000,
    }

    def __init__(self, wrap, timespec):
        FilterBase.__init__(self, wrap)
        m = self._timespec_rx.match(timespec)
        if not m:
            raise Exception('invalid time specification')
        s = int(m.group(1))
        if m.group(2):
            s *= self._mult[m.group(2).lower()]
        self._cutoff = time.time() - s

    def check(self, kind, msg):
        date_tuple = email.utils.parsedate_tz(msg['Date'])
        if not date_tuple:
            return msg, False
        t = email.utils.mktime_tz(date_tuple)
        return msg, t > self._cutoff


class UnwrapSpamReports(FilterBase):
    """Decodes multipart/report files and re-injects the wrapped message.

    Used to handle backscatter spam.
    """

    def check(self, kind, msg):
        if kind != 'spam':
            return msg, True
        if msg.get_content_type() != 'multipart/report':
            return msg, True
        for part in msg.walk():
            if part.get_content_type() == 'message/rfc822':
                s = part.get_payload()[0]
                inner_msg = email.message_from_string(str(s))
                log.info('multipart/report wrapped message %s',
                         inner_msg['Message-ID'] or inner_msg['MessageID'])
                return inner_msg, True
        # It was a multipart/report message but without a message...
        # Let's just ignore it.
        return msg, False


class CollectUnsubscribeLinks(FilterBase):
    """Collect all List-Unsubscribe links seen."""

    _link_rx = re.compile('<(https?://[^>]+)>')
    _space_rx = re.compile('\s+')

    def __init__(self, wrap, links):
        FilterBase.__init__(self, wrap)
        self._links = links

    def check(self, kind, msg):
        if kind != 'spam':
            return msg, True
        unsub_hdr = msg['List-Unsubscribe']
        if not unsub_hdr:
            return msg, True

        for m in self._link_rx.findall(unsub_hdr):
            url = self._space_rx.sub('', m)
            self._links.add(url)

        return msg, True
