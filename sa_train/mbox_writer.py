import logging
import mailbox

log = logging.getLogger(__name__)


class MboxWriter(object):

    def __init__(self, filename):
        self.mbox = mailbox.mbox(filename)
        self.filename = filename
        self.n_msgs = 0

    def write(self, msg):
        self.mbox.add(msg)
        self.n_msgs += 1

    def close(self):
        log.info(' - %s (%d messages)', self.filename, self.n_msgs)
        self.mbox.close()


class MultipleMboxWriter(object):

    def __init__(self, filename, max_msgs=5000):
        if '.mbox' in filename:
            self.fmt = filename.replace('.mbox', '.%04d.mbox')
        else:
            self.fmt = filename + '.%04d'
        self.cur = 0
        self.n_msgs = 0
        self.max_msgs = max_msgs
        self.mbox = None

    def write(self, msg):
        if self.mbox is None:
            self.mbox = mailbox.mbox(self.fmt % self.cur)
        self.mbox.add(msg)
        self.n_msgs += 1
        if self.n_msgs >= self.max_msgs:
            self.cur += 1
            self.n_msgs = 0
            self.mbox.close()
            self.mbox = None

    def close(self):
        if self.mbox is not None:
            self.mbox.close()


def write_to_mboxes(src, spam_mbox, ham_mbox, max_size=None):
    def _w(mboxfile):
        if max_size:
            return MultipleMboxWriter(mboxfile, max_size)
        else:
            return MboxWriter(mboxfile)
    writers = {
        'spam': _w(spam_mbox),
        'ham': _w(ham_mbox),
    }
    for kind, msg in src.scan():
        if kind not in writers:
            continue
        writers[kind].write(msg)
    src.close()
    for w in writers.values():
        w.close()
