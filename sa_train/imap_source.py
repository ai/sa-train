import email
import imaplib
import logging
import socket
import urlparse

log = logging.getLogger(__name__)


_conn_fn = {
    'imap': imaplib.IMAP4,
    'imaps': imaplib.IMAP4_SSL,
}

def imap_connection(conn_string):
    u = urlparse.urlsplit(conn_string)
    if u.scheme not in ('imap', 'imaps'):
        raise Exception('bad URL scheme %s' % u.scheme)
    if '@' not in u.netloc:
        raise Exception('no auth credentials in URL')
    userpass, server = u.netloc.rsplit('@', 1)
    if ':' not in userpass:
        raise Exception('malformed credentials (expected username:password)')
    username, password = userpass.split(':', 1)
    imap = _conn_fn[u.scheme](server)
    imap.login(username, password)
    imap.select('INBOX')
    return imap


class IMAPSource(object):
    """Read messages from an IMAP account."""

    def __init__(self, kind, conn_string):
        self.kind = kind
        self.conn_string = conn_string

    def close(self):
        pass

    def _connect(self):
        return imap_connection(self.conn_string)

    def scan(self):
        """Iterate over all messages in the IMAP mailbox.

        Will attempt to reconnect on errors.
        """
        imap = self._connect()
        rv, data = imap.search(None, 'ALL')
        if rv != 'OK':
            raise Exception('Could not list messages: %s' % rv)

        msgids = data[0].split()
        print 'found %d messages' % len(msgids)

        for n in msgids:
            try:
                rv, msg_data = imap.fetch(n, '(RFC822)')
                if rv != 'OK':
                    log.error('error fetching message %d: %s', n, rv)
                    continue
            except socket.gaierror as e:
                logging.error('error fetching message %d: %s -- reconnecting...', n, str(e))
                imap.close()
                imap = self._connect()
                continue

            msg = email.message_from_string(msg_data[0][1])
            yield (self.kind, msg)

        imap.logout()
        imap.close()
