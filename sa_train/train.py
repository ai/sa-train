import gnupg
import logging
import optparse
import os
import re

from sa_train import common
from sa_train import imap_source
from sa_train import mbox_source
from sa_train import mbox_writer


def _parse_raw_source(gpg, kind, s):
    if s.startswith('imap://') or s.startswith('imaps://'):
        return imap_source.IMAPSource(kind, s)
    return mbox_source.generic_mbox_source(gpg, kind, s)


def parse_source(gpg, s):
    # gpg:<SOURCE>
    if s.startswith('gpg:'):
        return common.GPGPipe(gpg, _parse_raw_source(gpg, 'unknown', s[4:]))

    # archive:<PATH>
    if s.startswith('archive:'):
        return common.ArchiveSource(gpg, s[8:])

    # ham:<PATH>
    # spam:<PATH>
    # <PATH> (defaults to spam)
    kind = 'spam'
    m = re.match(r'^(ham|spam):(.*)$', s)
    if m:
        kind = m.group(1)
        s = m.group(2)

    return _parse_raw_source(gpg, kind, s)


def main():
    def append_opt(option, opt, value, parser):
        getattr(parser.values, option.dest).append(value)

    parser = optparse.OptionParser()
    parser.add_option('--source', dest='sources', type='string',
                      default=[], action='callback', callback=append_opt)
    parser.add_option('--output-dir', metavar='DIR', dest='output_dir', default='.')
    parser.add_option('--sample', type='int', default=0, metavar='N',
                      help='Randomly sample a maximum of N messages per each input source')
    parser.add_option('--max-mbox-size', type='int', metavar='N',
                      help='Split the output in multiple mbox files, each '
                      'containing at most N messages')
    parser.add_option('--since', default='1y')
    opts, args = parser.parse_args()
    if args:
        parser.error('too many arguments')
    if not opts.sources:
        parser.error('no sources specified')

    logging.basicConfig(level=logging.INFO)

    gpg = gnupg.GPG(
        gpgbinary=os.getenv('GPG', '/usr/bin/gpg2'),
    )

    # Wrap all sources with a stats pipe.
    sources = [common.StatsPipe(x, parse_source(gpg, x))
               for x in opts.sources]

    # Collect list-unsubscribe links for emails that users report as
    # spam and click them all.
    unsub_links = set()

    mbox_writer.write_to_mboxes(
        common.RandomSample(
            common.CollectUnsubscribeLinks(
                common.UnwrapSpamReports(
                    common.Recent(
                        common.Concatenate(sources),
                        opts.since)),
                unsub_links),
            opts.sample),
        os.path.join(opts.output_dir, 'spam.mbox'),
        os.path.join(opts.output_dir, 'ham.mbox'),
        max_size=opts.max_mbox_size,
    )

    # Just print the unsubscribe links out, for now.
    if unsub_links:
        print 'Unsubscribe links:'
        for l in unsub_links:
            print l


if __name__ == '__main__':
    main()
