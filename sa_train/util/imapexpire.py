#!/usr/bin/python
"""
Delete old messages from a remote mailbox over IMAP. Server details
and credentials should be passed as a URI on the command line, for
instance:

    imapexpire imaps://user:password@server/

Expiration date can be set with the --days command-line parameter. By
default, messages older than 365 days will be deleted.
"""

import sys
import datetime
import imaplib
import optparse
import urlparse

from sa_train.imap_source import imap_connection


def chunks(l, n):
    for i in xrange(0, len(l), n):
        yield l[i:i+n]


def cleanup(conn, days):
    cutoff_date = (datetime.date.today() - datetime.timedelta(days)).strftime('%d-%b-%Y')
    typ, data = conn.search(None, '(BEFORE %s)' % cutoff_date)
    tot = 0
    for batch in chunks(data[0].split(), 100):
        conn.store(','.join(batch), '+FLAGS', '\\Deleted')
        tot += len(batch)
        sys.stderr.write('\rdeleting %d messages...   ' % tot)
    sys.stderr.write('\rexpunging folder...         ')
    conn.expunge()
    sys.stderr.write('\rdone.                       \n')


def main():
    parser = optparse.OptionParser(usage='%prog [<OPTIONS>] <URI>\n\n' + __doc__)
    parser.add_option('--days', type='int', default=365)
    opts, args = parser.parse_args()
    if len(args) != 1:
        parser.error('Wrong number of arguments')
    imap_uri = args[0]

    try:
        conn = imap_connection(imap_uri)
        cleanup(conn, opts.days)
        conn.close()
        conn.logout()
    except Exception as e:
        print >>sys.stderr, 'Error: %s' % str(e)
        sys.exit(1)


if __name__ == '__main__':
    main()

