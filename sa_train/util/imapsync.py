import optparse
import mailbox
import email
import imaplib

from sa_train.imap_source import imap_connection


def get_missing_imap_ids(conn, local_ids):
    rv, data = conn.search(None, 'ALL')
    if rv != 'OK':
        raise Exception('IMAP error: %s' % rv)
    for msgid in data[0].split():
        rv, msg_hdr = conn.fetch(msgid, '(RFC822.HEADER)')
        if rv != 'OK':
            raise Exception('IMAP error: %s' % rv)
        hdr = email.message_from_string(msg_hdr[0][1])
        if hdr['Message-Id'] not in local_ids:
            print 'fetching message %s' % hdr['Message-Id']
            rv, msg_data = conn.fetch(msgid, '(RFC822)')
            if rv != 'OK':
                raise Exception('error fetching message %s: %s' % (
                    hdr['Message-Id'], rv))
            yield email.message_from_string(msg_data[0][1])
        else:
            print 'skipping message %s' % hdr['Message-Id']


def sync(imap, mbox):
    local_ids = set()
    for msg in mbox:
        local_ids.add(msg['Message-Id'])
    print 'local mailbox has %d messages' % len(local_ids)
    for msg in get_missing_imap_ids(imap, local_ids):
        mbox.add(msg)


def main():
    parser = optparse.OptionParser(
        usage='%prog <IMAP_URI> <MBOX>')
    opts, args = parser.parse_args()
    if len(args) != 2:
        parser.error('wrong number of args')
    imap_uri, mbox_path = args

    imap = imap_connection(imap_uri)
    mbox = mailbox.mbox(mbox_path)
    sync(imap, mbox)
    mbox.close()


if __name__ == '__main__':
    main()
