/**
 * sa-report: sa-train feedback loop client.
 *
 * The desired classification is indicated by the --spam or --ham
 * command-line options. The program reads an email message on
 * standard input, and sends it (along with some metadata) to the
 * sa-report server over a UNIX socket.
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <pwd.h>
#include <getopt.h>
#include <unistd.h>
#include <errno.h>

#define BUFSIZE 16384

static struct option long_options[] = {{"help", no_argument, 0, 'h'},
                                       {"ham", no_argument, 0, 'H'},
                                       {"spam", no_argument, 0, 'S'},
                                       {"socket", required_argument, 0, 's'},
                                       {0, 0, 0, 0}};

char *kind = NULL;
char *username = NULL;
char *socket_path = "/run/sa-report/socket";

static void usage() {
  fprintf(stderr,
          "Usage: sa-report-client [<OPTIONS>] {--ham|--spam}\n"
          "Known options:\n"
          "   --help            Show this help message\n"
          "   --socket=PATH     Set the sa-report socket path (default %s)\n"
          "\n",
          socket_path);
}

static int connect_socket(int *out_sock) {
  struct sockaddr_un addr;

  int fd = socket(AF_UNIX, SOCK_STREAM, 0);
  if (fd < 0) {
    return -1;
  }

  memset(&addr, 0, sizeof(addr));
  addr.sun_family = AF_UNIX;
  strncpy(addr.sun_path, socket_path, sizeof(addr.sun_path) - 1);

  if (connect(fd, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
    fprintf(stderr, "connect error: %s: %s\n", socket_path, strerror(errno));
    close(fd);
    return -1;
  }

  *out_sock = fd;
  return 0;
}

static void parse_options(int argc, char **argv) {
  while (1) {
    struct option *opt;
    int option_index = 0;
    int c = getopt_long(argc, argv, "hHSs:", long_options, &option_index);
    if (c < 0) {
      break;
    }
    switch (c) {
    case 'h':
      usage();
      exit(0);

    case 's':
      socket_path = optarg;
      break;

    case 'H':
      if (kind != NULL) {
        fprintf(stderr, "Must specify only one of --ham and --spam\n");
        exit(1);
      }
      kind = "ham";
      break;

    case 'S':
      if (kind != NULL) {
        fprintf(stderr, "Must specify only one of --ham and --spam\n");
        exit(1);
      }
      kind = "spam";
      break;

    default:
      usage();
      exit(1);
    }
  }

  if (kind == NULL) {
    usage();
    exit(1);
  }
}

static int write_all(int sock, char *buf, int n) {
  int w = 0;
  while (w < n) {
    int ww = write(sock, buf + w, n - w);
    if (ww < 0) {
      return -1;
    }
    w += ww;
  }
  return 0;
}

static int copy_stdin(int sock) {
  char *buf;
  int n, ret = 0;

  // Copy from stdin to socket.
  buf = (char *)malloc(BUFSIZE);
  while ((n = read(0, buf, BUFSIZE)) > 0) {
    if (write_all(sock, buf, n) < 0) {
      ret = -1;
      break;
    }
  }

  free(buf);
  return ret;
}

static int write_line(int sock, char *line) {
  char *buf;
  int l, n, ret;

  l = strlen(line);
  buf = (char *)malloc(l + 2);
  sprintf(buf, "%s\n", line);

  ret = write_all(sock, buf, l + 1);
  free(buf);
  return ret;
}

static int forward_message() {
  int ret = 0, n, sock;

  if (connect_socket(&sock) < 0) {
    return -1;
  }

  (void)write_line(sock, username);
  (void)write_line(sock, kind);

  if (copy_stdin(sock) < 0) {
    fprintf(stderr, "message copy failed\n");
    ret = -1;
  }

err:
  close(sock);
  return ret;
}

static int get_username() {
  struct passwd *u;

  u = getpwuid(getuid());
  if (u == NULL) {
    fprintf(stderr, "can't get username\n");
    return -1;
  }
  username = strdup(u->pw_name);
  return 0;
}

int main(int argc, char **argv) {
  int ret = 0;

  parse_options(argc, argv);

  if (get_username() < 0) {
    ret = 1;
    goto err;
  }

  if (forward_message() < 0) {
    ret = 1;
    goto err;
  }

err:
  if (username != NULL)
    free(username);
  return ret;
}
