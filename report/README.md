
This directory contains code for a client/server implementation of the
mis-classified messages feedback loop.

In our implementation of the loop, messages mis-classified as spam or
ham are collected in a mailbox (encrypted), along with their correct
classification. A batch process then reads these messages and builds a
new Bayesian token map for Spamassassin.

The loop is started by the dovecot-antispam plugin, which spawns a
program whenever a user moves a message to or from the *Spam*
folder. Since this program is executed synchronously within the folder
move, and considering that it is spawned by dovecot in a very
resource-constrained context, it has to be quick and tiny, deferring
all processing and queuing behavior to a separate component. Such
component is the sa-report-server, which takes care of performing PGP
encryption and forwarding the message to the collector mailbox.

