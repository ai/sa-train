// sa-report-server
//
// Listens on a UNIX socket for messages coming from the
// dovecot-antispam plugin, and forwards them to a collector over SMTP
// in a specially wrapped (encrypted) format.
//
// The protocol spoken over the UNIX socket is simple and line-based:
//
// First line: originating address (from dovecot)
// Second line: desired classification (ham/spam)
// The full text of the message follows.
//
// To test, "socat -u STDIN UNIX-CONNECT:path/to/socket".
//
package main

import (
	"bufio"
	"bytes"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net"
	"net/smtp"
	"os"
	"os/exec"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"golang.org/x/crypto/openpgp"
	"golang.org/x/crypto/openpgp/armor"
)

var (
	socketPath   = flag.String("socket", "/run/sa-report/socket", "UNIX socket path to listen on")
	maxQueueSize = flag.Int("queue-size", 1000, "maximum queue size")
	numWorkers   = flag.Int("num-workers", 1, "number of cores to use")
	pgpKeyPath   = flag.String("pgp-key-path", "", "path to PGP public key (armored)")
	forwardAddr  = flag.String("addr", "", "address to forward encrypted emails to")
	smtpServer   = flag.String("smtp-server", "", "SMTP server to use - if empty, uses /usr/sbin/sendmail")

	connTimeout = 10 * time.Second
)

type message struct {
	From string
	Kind string
	Body []byte
}

type server struct {
	keyring     []*openpgp.Entity
	forwardAddr string
}

func newServer(keyPath, fwdAddr string) (*server, error) {
	f, err := os.Open(keyPath)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	el, err := openpgp.ReadArmoredKeyRing(f)
	if err != nil {
		return nil, err
	}

	if len(el) == 0 {
		return nil, fmt.Errorf("no public keys found")
	}

	return &server{keyring: el, forwardAddr: fwdAddr}, nil
}

func (s *server) encryptMessage(w io.Writer, msg *message) error {
	aw, err := armor.Encode(w, "PGP MESSAGE", nil)
	if err != nil {
		return fmt.Errorf("armor error: %v", err)
	}
	defer aw.Close()
	ew, err := openpgp.Encrypt(aw, s.keyring, nil, nil, nil)
	if err != nil {
		return fmt.Errorf("encrypt error: %v", err)
	}
	defer ew.Close()
	ew.Write(msg.Body)
	return nil
}

func (s *server) sendMessage(sender, rcpt string, body []byte) error {
	var err error
	for i := 0; i < 10; i++ {
		err = smtp.SendMail(*smtpServer, nil, sender, []string{rcpt}, body)
		if err == nil {
			break
		}
		time.Sleep(1 * time.Second)
	}
	return err
}

func (s *server) sendMessageWithSendmail(sender, rcpt string, body []byte) error {
	cmd := exec.Command("/usr/sbin/sendmail", "-f", sender, "-i", rcpt)
	cmd.Stdin = bytes.NewReader(body)
	return cmd.Run()
}

func (s *server) processMessage(msg *message) {
	log.Printf("message from %s re-classified as %s", msg.From, msg.Kind)

	var buf bytes.Buffer
	fmt.Fprintf(&buf, "From: %s\r\n", msg.From)
	fmt.Fprintf(&buf, "Subject: %s\r\n", msg.Kind)
	fmt.Fprintf(&buf, "To: %s\r\n\r\n", s.forwardAddr)

	if err := s.encryptMessage(&buf, msg); err != nil {
		log.Printf("error: %v", err)
		return
	}

	var err error
	if *smtpServer != "" {
		err = s.sendMessage(msg.From, s.forwardAddr, buf.Bytes())
	} else {
		err = s.sendMessageWithSendmail(msg.From, s.forwardAddr, buf.Bytes())
	}
	if err != nil {
		log.Printf("error sending message: %v", err)
	}
}

func main() {
	log.SetFlags(0)
	flag.Parse()

	if *pgpKeyPath == "" {
		log.Fatal("Must specify --pgp-key-path")
	}
	if *forwardAddr == "" {
		log.Fatal("Must specify --addr")
	}

	srv, err := newServer(*pgpKeyPath, *forwardAddr)
	if err != nil {
		log.Fatal(err)
	}

	l, err := net.Listen("unix", *socketPath)
	if err != nil {
		log.Fatal(err)
	}

	// Terminate cleanly on SIGTERM and others.
	sigCh := make(chan os.Signal, 1)
	go func() {
		s := <-sigCh
		log.Printf("received %s signal, exiting...", s)
		l.Close()
	}()
	signal.Notify(sigCh, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)

	var wg sync.WaitGroup

	// Start background workers.
	ch := make(chan *message, *maxQueueSize)
	for i := 0; i < *numWorkers; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			for msg := range ch {
				srv.processMessage(msg)
			}
		}()
	}

	// Accept incoming connections in a loop and spawn goroutines.
	log.Printf("spam reporting daemon started")
	for {
		c, err := l.Accept()
		if err != nil {
			break
		}

		wg.Add(1)
		go func(c net.Conn) {
			defer wg.Done()
			defer c.Close()

			c.SetDeadline(time.Now().Add(connTimeout))
			r := bufio.NewReader(c)
			firstLine, err := r.ReadBytes('\n')
			if err != nil {
				log.Printf("from %s: error reading first line: %v", c.RemoteAddr(), err)
				return
			}

			secondLine, err := r.ReadBytes('\n')
			if err != nil {
				log.Printf("from %s: error reading second line: %v", c.RemoteAddr(), err)
				return
			}

			body, err := ioutil.ReadAll(r)
			if err != nil {
				log.Printf("from %s: error reading body: %v", c.RemoteAddr(), err)
				return
			}

			msg := &message{
				From: string(bytes.TrimSpace(firstLine)),
				Kind: string(bytes.TrimSpace(secondLine)),
				Body: body,
			}

			select {
			case ch <- msg:
			default:
				log.Printf("queue is overloaded, dropping incoming message")
			}
		}(c)
	}

	close(ch)
	wg.Wait()
	log.Printf("terminating")
}
